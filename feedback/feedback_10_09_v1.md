Documents Feedback Team - 16
===========================
 
### Project Concept (6/10)
    - The concept is not explained in detail.
    - Moreover the usage model and diagrams are not updated.
	 
### Project Plan (7/10)
    - Incomplete project plan. 
		    
### Status Tracker 1 (4/5)
    - Improve neatness in the doc.
    
### Status Tracker 2 (5/5)
    - Good. 
    
### Status Tracker 3 (3/5)
    - Only few members are assigned work.
    - Dates aren't menioned.
    - Individual status is not filled.
    
### Status Tracker 4 ( 4/5)
    - Individual status is not filled.
    
### Status Tracker 5 ( 5/5)
    - For uploading before I intimate you :)
    
### Status Tracker 6 ( 5/5)
    - Well Done
    
### Project Test Plan ( 10/10)
    - Planned well. 
    
### SRS ( 29/30)
    - Very well documented
    
### R1 feedback

    - Make sure you do well for R2.
    - Code and commit regularly.
    - Please put all minutes of minutes.
    
### Design Document (14/15)
    - Well done.
    
### R2 feedback
    - Imporved a lot after R1.
    - Project was mainly done in after R1.
    - Overall performance was pretty good.
    - Could overcomne the difficulties faced due to some reasons.

