Project: Tangram(Block-puzzle) game
Team: 16
Members: Vishal Batchu, Raghuram Vadapalli, Syamantak Das, Nikhil Tadigoppula
Client: Playzio

Technologies Used:
1)Cocos2d-js
2)Cocos Code IDE/Android Studio
3)Adobe Photoshop (For image generation)

Description:
Tangram is dissection puzzle game where the objective of the game is to put together the pieces to form a given shape.
Some of the features that we have are rotation of blocks in all game modes, unique game modes such as trick and double and a random level generator to keep players busy.
It also comes with a score tracker and achievements so you can try challenge your friends and be engaged with the game.

Future Ideas/Plans:
1)Add social integration.
2)Improve UI of the leaderboard and achievement sections. 

To edit/build from source: (Refer to the link mentioned below for detailed information)
1)Install cocos2d-js along with android-ndk,android-sdk and ant.
2)Using the cocos-code IDE, open the project and click "Run in browser" or "Android run" to either run the game in the browser on 127.0.0.1:8000 or on an android device.

To play the game:
1)Copy the tangram-game/tangram-game.apk file onto your phone and then install it.
2)You can then play the game on any android device.

References:
1)To learn more about how the level generation works (Cellular Automaton), "https://en.wikipedia.org/wiki/Cellular_automaton"
2)Installing an apk on a phone, "https://www.androidpit.com/android-for-beginners-what-is-an-apk-file"
3)Setting up cocos environment, "http://www.cocos2d-x.org/docs/tutorial/framework/html5/parkour-game-with-javascript-v3.0/chapter1/en"